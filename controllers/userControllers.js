
const User = require("./../models/User");
const Course = require('./../models/Courses');
const bcrypt = require('bcrypt');
const auth = require('./../auth');


module.exports.checkEmailExists = (reqBody) => {
	// console.log(reqBody)
	//model.method
	return User.find({email: reqBody.email})
	.then( (result) => {
		if(result.length != 0){ 
			return true
		} else {
			return false
		}
	})
}

module.exports.register = (reqBody) => {

	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		password: bcrypt.hashSync(reqBody.password, 10)
	})

	return newUser.save().then( (result, error) =>{
		if(error){
			return error
		} else {
			return true
		}
	})
} 


module.exports.login = (reqBody) => { 
	//Model.method
	return User.findOne({email: reqBody.email}).then( (result) => {

		if(result == null){
			return false

		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password) //boolean return

			if(isPasswordCorrect === true){
				return { access: auth.createAccessToken(result.toObject())}
			} else {
				return false
			}
		}
	})
}


module.exports.getProfile = (data) => {
	//console.log(data)
	//Model.method
	return User.findById(data).then( result => {

		result.password = "******"
		return result
	})
}

module.exports.enroll = async (data) => {

	//save user enrollments
	const userSaveStatus = await User.findById(data.userId).then( user => {
		// console.log(user)
		user.enrollments.push({courseId: data.courseId})

		return user.save().then( (user, error) => {
			if(error){
				return false
			} else {
				return true
			}
		})
	})

	//save course enrollees
	const courseSaveStatus = await Course.findById(data.courseId).then( course => {
		course.enrollees.push({userId: data.userId})

		return course.save().then( (course, error) => {
			if(error){
				return false
			} else {
				return true
			}
		})
	})


	if(userSaveStatus && courseSaveStatus){
		return true
	} else {
		return false
	}
}